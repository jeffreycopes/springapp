jQuery(function() {
   jQuery('.error').hide();
   jQuery(".button").click(function() {
      jQuery('.error').hide();
      var name = $("input#name").val();
      if (name == "") {
         jQuery("label#name_error").show();
         jQuery("input#name").focus();
         return false;
      }
      var email = jQuery("input#email").val();
      if (email == "") {
         jQuery("label#email_error").show();
         jQuery("input#email").focus();
         return false;
      }
      var phone = jQuery("input#phone").val();
      if (phone == "") {
         jQuery("label#phone_error").show();
         jQuery("input#phone").focus();
         return false;
      }
      console.log('name=' + name + '&email=' + email + '&phone=' + phone);
   });
});

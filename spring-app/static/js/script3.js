function myFunction() {
    var message, x;
    message = document.getElementById("message");
    message.innerHTML = "";
    x = document.getElementById("demo").value;
    try {
        if(x == "") throw "empty";
        if(isNaN(x)) throw "not a number";
        x = Number(x);
        if(x < 5) throw "too low";
        if(x > 10) throw "too high";
        if ((x >= 5)  && ( x <= 10)) {
              document.getElementById("result").innerHTML = x;
        }
    }
    catch(err) {
        message.innerHTML = "Input is " + err;
    }
}

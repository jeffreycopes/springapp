package org.bluemoonweather.springapp.test;

import junit.framework.TestCase;

import org.bluemoonweather.springapp.domain.Product;

public class ProductTest extends TestCase {
   private Product theProduct;

   protected void setUp() throws Exception {
      this.theProduct = new Product();
   }

   public void testSetAndGetTheDescription() {
      String testDescription = "aDescription";
      assertNull(this.theProduct.getTheDescription());
      this.theProduct.setTheDescription(testDescription);
      assertEquals(testDescription, this.theProduct.getTheDescription());
   }

   public void testSetAndGetThePrice() {
      double testPrice = 100.00;
      assertEquals(0, 0, 0);    
      this.theProduct.setThePrice(testPrice);
      assertEquals(testPrice, this.theProduct.getThePrice(), 0);
   }
}

package org.bluemoonweather.springapp.domain;

import java.io.Serializable;

public class Product implements Serializable {

    private String theDescription;
    private Double thePrice;
    
    public String getTheDescription() {
        return this.theDescription;
    }
    
    public void setTheDescription(String description) {
        this.theDescription = description;
    }
    
    public Double getThePrice() {
        return this.thePrice;
    }
    
    public void setThePrice(Double price) {
        this.thePrice = price;
    }
    
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Description: " + this.theDescription + ";");
        buffer.append("Price: " + this.thePrice);
        return buffer.toString();
    }
}

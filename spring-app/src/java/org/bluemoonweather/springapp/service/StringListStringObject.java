package org.bluemoonweather.springapp.service;

import org.apache.log4j.Logger;

public class StringListStringObject {
   final static Logger logger = 
      Logger.getLogger(StringListStringObject.class);

   private String theStringListString;

   public void setTheStringListString(String stringListString) {
      this.theStringListString = stringListString;
   }

   public String getTheStringListString() {
      return this.theStringListString;
   }
}

package org.bluemoonweather.springapp.service;

import org.springframework.validation.Validator;
import org.springframework.validation.Errors;

import org.apache.log4j.Logger;

public class StringListStringObjectValidator implements Validator {
   final static Logger logger = 
      Logger.getLogger(StringListStringObjectValidator.class);

   public boolean supports(Class class_) {
      return StringListStringObject.class.equals(class_);
   }

   public void validate(Object object, 
                        Errors errors) {
      StringListStringObject stringListStringObject = 
         (StringListStringObject) object;
   }
}

package org.bluemoonweather.springapp.service;

import java.io.Serializable;
import java.util.ArrayList;

public interface StringListManager extends Serializable{
    public void setTheStringList(ArrayList<String> stringList);
    public ArrayList<String> getTheStringList();
    public void processStringListString(String stringListString);
}

/*************************************************************************************************
HISTORY
DATE            AUTHOR          DESCRIPTION
20170806        Jeff Copes      fixed setTheStringList method
*************************************************************************************************/
package org.bluemoonweather.springapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.apache.log4j.Logger;

public class SimpleStringListManager implements StringListManager {
   final static Logger logger =
      Logger.getLogger(SimpleStringListManager.class);

   private ArrayList<String> theStringList;

   public SimpleStringListManager() {
      this.theStringList = new ArrayList<String>();
   }

   public ArrayList<String> getTheStringList() {
      return this.theStringList;
   }

   public void setTheStringList(ArrayList<String> stringList) {
      this.theStringList = stringList;
   }

   public void processStringListString(String stringListString) {
      createList(stringListString);
      sendRequests();
   }
 
   private void createList(String stringListString) {
      List<String> stringList = new ArrayList<String>();
      String[] stringArray = stringListString.split(",");
      stringList = Arrays.asList(stringArray);
      int stringListSize = stringList.size();
      for (int idx = 0; idx < stringListSize; idx++) {
         String string = stringList.get(idx).trim();
         this.theStringList.add(string);
      }
   }

   private void sendRequests() {
      for (String string : this.theStringList) {
         sendRequest(string);
      }
   }

   private void sendRequest(String string) {
      logger.info(string);
   }
}

/*************************************************************************************************
HISTORY
DATE            AUTHOR          DESCRIPTION
20170806        Jeff Copes      used correct SimpleStringListManagerTest::setTheStringList method
*************************************************************************************************/
package org.bluemoonweather.springapp.service;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.bluemoonweather.springapp.service.SimpleStringListManager;

public class SimpleStringListManagerTest extends TestCase {

   private SimpleStringListManager theSimpleStringListManager;
   private ArrayList<String> theStringList;
    
   private static int PRODUCT_COUNT = 2;
   private static String CHAIR_DESCRIPTION = "aa,bb,cc";
   private static String TABLE_DESCRIPTION = "xx,yy,zz";
        
   protected void setUp() throws Exception {
      this.theSimpleStringListManager = new SimpleStringListManager();
      this.theStringList = new ArrayList<String>();
        
      // stub up a list of products
      this.theStringList.add("aa,bb,cc");
      this.theStringList.add("xx,yy,zz");
      
      this.theSimpleStringListManager.setTheStringList(this.theStringList);
   }

   public void testGetStringLists() {
      ArrayList<String> stringList = this.theSimpleStringListManager.getTheStringList();
      assertNotNull(stringList);        
      assertEquals(PRODUCT_COUNT, this.theSimpleStringListManager.getTheStringList().size());
  
      String string = stringList.get(0);
      assertEquals(CHAIR_DESCRIPTION, string);
      string = stringList.get(1);
      assertEquals(TABLE_DESCRIPTION, string);
   }   
}

/*************************************************************************************************
HISTORY
DATE            AUTHOR          DESCRIPTION
20170806        Jeff Copes      using logical name of view in junit assertEquals call
20170806        Jeff Copes      moved to org.bluemoonweather.springapp.test
20170806	Jeff Copes	changed to receive Map from ModelAndView object
                                returned by handleRequest()
20170808	Jeff Copes	renamed from SpringAppControllersTest to SpringAppControllerTest
*************************************************************************************************/
package org.bluemoonweather.springapp.test;

import java.util.Map;
import java.util.HashMap;

import org.springframework.web.servlet.ModelAndView;

import org.bluemoonweather.springapp.web.SpringAppController;
//import org.bluemoonweather.springapp.service.SimpleProductManager;

import junit.framework.TestCase;

public class SpringAppControllerTest extends TestCase {

   public void testHandleRequestView() throws Exception{		
      final String viewName = new String("springapp");
      final String nowKey = new String("now");
      final String modelKey = new String("model");

      SpringAppController springAppController = new SpringAppController();
//      SimpleProductManager simpleProductManager = new SimpleProductManager();
//      springAppController.setTheProductManager(simpleProductManager);

      ModelAndView modelAndView = springAppController.handleRequest(null, null);		
      assertEquals(viewName, modelAndView.getViewName());
      assertNotNull(modelAndView.getModel());

      Map<String,Object> modelMap = (Map<String,Object>)(modelAndView.getModel().get(modelKey));
      String nowValue = (String) modelMap.get(nowKey);
      assertNotNull(nowValue);
   }
}

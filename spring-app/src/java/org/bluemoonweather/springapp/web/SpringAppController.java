/*************************************************************************************************
HISTORY
DATE		AUTHOR		DESCRIPTION
20170806	Jeff Copes	using logical name of view in return value of handleRequest
20170806	Jeff Copes	moved to org.bluemoonweather.springapp.web
20170806	Jeff Copes	changed handleRequest to initialize ModelAndView with HashMap
20170808	Jeff Copes	renamed from SpringAppControllers to SpringAppController
*************************************************************************************************/
package org.bluemoonweather.springapp.web;

import java.io.IOException;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class SpringAppController implements Controller {
   final static Logger logger =
      Logger.getLogger(SpringAppController.class);

   public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
      final String viewName = new String("springapp");
      final String nowStringKey = new String("now");
//      final String productListKey = new String("products");

      final Date nowDate = new Date();
      final String nowString = nowDate.toString();
//      final List<Product> productList = theProductManager.getTheProductList();

      Map<String,Object> modelMap = new HashMap<String,Object>();

      modelMap.put(nowStringKey,
                   nowString);
//      modelMap.put(productListKey,
//                   productList);

      logger.info("springapp view with " + nowString);
      return new ModelAndView(viewName, "model", modelMap);
   }

//   public void setTheProductManager(ProductManager productManager) {
//      this.theProductManager = productManager;
//   }

}

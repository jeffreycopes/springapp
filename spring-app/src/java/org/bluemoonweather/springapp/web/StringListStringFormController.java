package org.bluemoonweather.springapp.web;

import java.util.Date;

import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import org.bluemoonweather.springapp.service.StringListManager;
import org.bluemoonweather.springapp.service.SimpleStringListManager;
import org.bluemoonweather.springapp.service.StringListStringObject;

public class StringListStringFormController extends SimpleFormController {

   final static Logger logger = 
      Logger.getLogger(StringListStringFormController.class);

   private StringListManager theStringListManager;

   public ModelAndView onSubmit(Object command)
      throws ServletException {
      String stringListString = 
         ((StringListStringObject) command).getTheStringListString();
      this.theStringListManager = new SimpleStringListManager();
      this.theStringListManager.processStringListString(stringListString);
      logger.info("Returning " + getSuccessView() + " view from " + getFormView() + " view");
      return new ModelAndView(new RedirectView(getSuccessView()));
   }

   protected Object formBackingObject(HttpServletRequest request) throws ServletException {
      final Date dateNow = new Date();
      final String dateNowString = dateNow.toString();
      StringListStringObject stringListStringObject = 
         new StringListStringObject();
      stringListStringObject.setTheStringListString(dateNowString);
      return stringListStringObject;
   }

   public void setStringListManager(StringListManager stringListManager) {
      this.theStringListManager = stringListManager;
   }

   public StringListManager getTheStringListManager() {
      return this.theStringListManager;
   }
}

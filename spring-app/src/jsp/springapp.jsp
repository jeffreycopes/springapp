<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!--
***********************************************************************************************
DATE		AUTHOR		DESCRIPTION
20170807	Jeff Copes	Fixed Product attribute names
20170808	Jeff Copes	Added URL link for price increase controller
***********************************************************************************************
-->

<html>
  <head><title><fmt:message key="title"/></title></head>
  <body>
    <h1><fmt:message key="heading"/></h1>
    <p><fmt:message key="greeting"/> <c:out value="${model.now}"/></p>
<!--
    <c:forEach items="${model.products}" var="prod">
      <c:out value="${prod.theDescription}"/> <i>$<c:out value="${prod.thePrice}"/></i><br><br>
    </c:forEach>
-->
    <br>
    <a href="<c:url value="stringliststringinput.htm"/>"><h3>Enter String</h3></a>
    <br>
  </body>
</html>
